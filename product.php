<?php
include("connection.php");

$mail=isset($_COOKIE['mail']) ? $_COOKIE['mail'] :  "" ;

$sql = "select * from register where mail='$mail'";
	$result = mysqli_query($con, $sql);
	$no = mysqli_num_rows($result);
	$row = mysqli_fetch_row($result);

 // print_r($row);die();
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lamination company</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>

  <!-- ======= Top Bar ======= -->
  <!--div id="topbar" class="fixed-top d-flex align-items-center ">
    <div class="container d-flex align-items-center justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope-fill"></i><a href="mailto:contact@example.com">rtcgabi@yahoo.com</a>
        <i class="bi bi-phone-fill phone-icon"></i>  04562-223170
      </div>
      <div class="cta d-none d-md-block">
        <a href="#about" class="scrollto">Get Started</a>
      </div>
    </div>
  </div-->


  <!------Navbar-------->

<?php include("nav.php"); ?>
  <div class="row col-md-12">
  <div class="col-lg-4  col-md-4 ">
  <div class="card">
  <div class="card-header">
 Lamination
  </div>
     <div class="card-body">
    <blockquote class="blockquote mb-0">
      <p> 3.7 x 5.2 inches <span class="btn btn-danger btn-sm">Rs. 500</span></p>  
      <p> 2.3 x 3.7 inches <span class="btn btn-danger btn-sm">Rs. 800</span></p>  
      <p> 2.5 x 3.8 inches <span class="btn btn-danger btn-sm">Rs. 900</span></p>  
    </blockquote>
  </div>
  </div>
</div>

<div class="col-lg-4  col-md-4  col-12">
  <div class="card">
  <div class="card-header">
  Lamination gum
  </div>
     <div class="card-body">
    <blockquote class="blockquote mb-0">
    <p> 55 kg <span class="btn btn-danger btn-sm">Rs. 200</span></p>  
      <p> 75 kg <span class="btn btn-danger btn-sm">Rs. 500</span></p>  
      <p> 90 Kg <span class="btn btn-danger btn-sm">Rs. 800</span></p>  
</blockquote>
  </div>
  </div>
</div>


<div class="col-lg-4  col-md-4  col-12">
  <div class="card">
  <div class="card-header">
  Printing ink
  </div>
     <div class="card-body">
    <blockquote class="blockquote mb-0">
    <p> Proton Printing Ink <span class="btn btn-danger btn-sm">Rs. 500</span></p>  
      <p> CSC Ecosol E1 White Ink <span class="btn btn-danger btn-sm"> Rs. 500</span></p>  
      <p> Digital Printing <span class="btn btn-danger btn-sm">Rs. 700</span></p>    </blockquote>
  </div>
  </div>
</div>
  </div>
<section id="product">
    <div class="container  m-5">
      <h1 class="text-center  my-5">OUR PRODUCTS</h1>
      <div class="row">
        <div class="col-lg-4  col-md-4  col-12">
          <div class="card">
            <img src="BOPP film.jpg"  class="card-img-top">
            <div class="card-body">
            <h5 class="card-title"> BOPP film</h5>
            <?= (!empty($mail)) ? '<a href="buy_lamination.php" class="btn register">Buy Now</a>' : '<a href="login.php" class="btn register">Buy Now</a>' ?>
             </div>
        </div>
      </div>

      <div class="col-lg-4  col-md-4  col-12">
       <div class="card">
         <img src="lamination gum.png"  class="card-img-top">
         <div class="card-body">
         <h5 class="card-title"> Lamination gum</h5>
         <?= (!empty($mail)) ? '<a href="buy_gum.php" class="btn register">Buy Now</a>' : '<a href="login.php" class="btn register">Buy Now</a>' ?>
           </div>
     </div>
   </div>

   <div class="col-lg-4  col-md-4  col-12">
     <div class="card">
       <img src="lamination.png"  class="card-img-top">
       <div class="card-body  text-center">
       <h5 class="card-title">Lamination</h5>
       <?= (!empty($mail)) ? '<a href="buy_lamination.php" class="btn register">Buy Now</a>' : '<a href="login.php" class="btn register">Buy Now</a>' ?>
              </div>
   </div>
 </div>
    </div>
    
  <div class="row">
   <div class="col-lg-4  col-md-4  col-12">
     <div class="card">
       <img src="matt film.png"  class="card-img-top">
       <div class="card-body text-center">
       <h5 class="card-title"> Matt film</h5>
       <?= (!empty($mail)) ? '<a href="buy_lamination.php" class="btn register">Buy Now</a>' : '<a href="login.php" class="btn register">Buy Now</a>' ?>
             </div>
   </div>
 </div>

 <div class="col-lg-4  col-md-4  col-12">
  <div class="card">
    <img src="paper sheet.png"  class="card-img-top">
    <div class="card-body  text-center">
    <h5 class="card-title"> Paper sheet</h5>
    <?= (!empty($mail)) ? '<a href="buy_lamination.php" class="btn register">Buy Now</a>' : '<a href="login.php" class="btn register">Buy Now</a>' ?>
           </div>
</div>
</div>

<div class="col-lg-4  col-md-4  col-12">
<div class="card">
 <img src="printing ink(yellow,blue,magenta,black).png"  class="card-img-top">
 <div class="card-body">
 <h5 class="card-title">Printing ink</h5>
 <?= (!empty($mail)) ? '<a href="buy_ink.php" class="btn register">Buy Now</a>' : '<a href="login.php" class="btn register">Buy Now</a>' ?>
          </div>
</div>
</div>
</div>
</div>
</section>


<?php include("footer.php"); ?>
</body>
</html>