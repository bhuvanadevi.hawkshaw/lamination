    <!-- ======= Footer ======= -->
    <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>Lamination Company</h3>
              <p>
                1151,P.K.S.A.Arumugam Road <br>
                Sivakasi-626123<br><br>
                <strong>Phone:</strong> 04562 223170<br>
                <strong>Email:</strong> rtcgabi@yahoo.com<br>
              </p>
              <!-- <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
              </div> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="about.php">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="product.php">Product</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="contact.php">Contact</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="<?= (!empty($mail)) ? 'buy_lamination.php': 'regitser.php' ?>">BOPP film</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?= (!empty($mail)) ? 'buy_gum.php': 'regitser.php' ?>">Lamination gum</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?= (!empty($mail)) ? 'buy_lamination.php': 'regitser.php' ?>">Lamination</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?= (!empty($mail)) ? 'buy_lamination.php': 'regitser.php' ?>">Matt film</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?= (!empty($mail)) ? 'buy_lamination.php': 'regitser.php' ?>">Paper sheet</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?= (!empty($mail)) ? 'buy_ink.php': 'regitser.php' ?>">Printing ink</a></li>
            </ul>
          </div>

          <!-- <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>

          </div> -->

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Lamination Company</span></strong>. All Rights Reserved
      </div>
      <!-- <div class="credits">
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div> -->
    </div>
  </footer><!-- End Footer -->
